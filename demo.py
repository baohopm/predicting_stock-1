import csv
import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt


dates = []
prices = []


def get_data(filename):
    with open(filename, 'r') as file:
        csvFileReader = csv.reader(file)
        next(csvFileReader)
        for row in csvFileReader:
            print(row[0])
            print(row[1])
            if row[1] != 'null':
                dates.append(int(row[0].split('-')[0] + row[0].split('-')[1] + row[0].split('-')[2]))
                prices.append(float(row[1]))

    return


# svr_lin = SVR(kernel='linear', C=1e3)
# svr_poly = SVR(kernel='poly', C=1e3, degree=2)
svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)


def fit_model(dates, prices):
    dates = np.reshape(dates, (len(dates), 1))

    print('Fitting time!!!')
    # print('    Linear model')
    # svr_lin.fit(dates, prices)
    # print('    Polynomial model')
    # svr_poly.fit(dates, prices)
    print('    RFB model\n')
    svr_rbf.fit(dates, prices)

    plt.scatter(dates, prices, color='black', label="data")
    # plt.plot(dates, svr_lin.predict(dates), color='green', label='Linear model')
    # plt.plot(dates, svr_poly.predict(dates), color='blue', label='Polynomial model')
    plt.plot(dates, svr_rbf.predict(dates), color='red', label='RBF model')
    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.title('Support Vector Regression')
    plt.legend()

    return
        # svr_rbf.predict(x)[0], svr_lin.predict(x)[0], svr_poly.predict(x)[0]


def predict_prices(x):
    return svr_rbf.predict(x)[0]

get_data('AAPL.csv')

fit_model(dates, prices)

plt.show()

input_date = input('Enter a date: ')

while input_date != 'quit':
    pred_date = int(input_date)

    pred = predict_prices([[pred_date]])
    print(pred)

    input_date = input('Enter a date: ')
